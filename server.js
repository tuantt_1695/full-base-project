import express from 'express';
import mongoose from 'mongoose';

import { db } from './config';
import routes from './routes';
import middlewares from './middlewares';

import helpers from './helpers/index';

helpers.responseData();

const app = express();
const port = process.env.PORT || 7001;

middlewares(app);
routes(app);

app.listen(port, () => console.log(`Your API Server is running on port ${ port }`));

// MongoDB connect
mongoose.Promise = global.Promise;
mongoose.connect(db.mongodb.uri, { useMongoClient: true })
    .then(() => console.log('MongoDB connected!'))
    .catch(err => console.log(err));

export default app;
