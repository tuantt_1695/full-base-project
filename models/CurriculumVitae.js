import mongoose, { Schema } from 'mongoose'
import { CurriculumVitae } from '../constants/model'
const { ObjectId } = Schema.Types
const curriculumVitaeSchema = new Schema({
    public: Boolean,
    user: {
        name: String
    },
    title: String,
    description: String,
    skills: [String],
    yearsOfExperience: Number
}, {
    timestamps: true
});

module.exports = mongoose.model("CurriculumVitae", curriculumVitaeSchema);
