const _ = require('lodash')

module.exports = _.merge(
    require('./base'),
    require(`./${process.env.NODE_ENV || 'development'}`)
)
