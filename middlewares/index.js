import bodyParser from 'body-parser'
import cors from 'cors'
import compression from 'compression'
import morgan from 'morgan'

export default function(app){

    // Log request to console
    app.use(morgan('dev'));

    // Cross Origin
    app.use(cors());

    // Gzip respone
    app.use(compression());

    // Parse post body
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

}
