const systemData = require('./responseData');
const allModel = require('./modelData');
const allRepo = require('./repositories');
const allCon = require('./controllers');
module.exports.responseData= function(){
    global.SystemData = systemData;
    allModel.globalAllModels();
    allRepo.globalAllRepo();
    allCon.globalAllCon();
};
