
const systemData ={
    success: (res,data)=>{
        return res.json({
            success:true,
            result:data
        })
    },
    fails: (res,error)=>{
        return res.json({
            success:false,
            error
        })
    }
}
module.exports = systemData;